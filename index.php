<?php

$content_rss = file_get_contents("https://www.liga.net/news/culture/rss.xml");
$items_rss = new SimpleXMLElement($content_rss);

foreach ($items_rss->channel->item as $key => $item_rss) {
    $title = $item_rss->title;
    $img = $item_rss->enclosure["url"];
    $descr = $item_rss->description;
    $newsLink = $item_rss->link;

    echo "<div>";
    echo "<h2>" . $title ."</h2>";
    echo "<img src='". $img . "'>";
    echo "<p>" . $descr . "</p>";
    echo "<a href='$newsLink'> <input type='button' value='News Link'> </a>" . "<hr>";
    echo "</div>";
}